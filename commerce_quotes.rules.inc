<?php
/**
 * @file
 * commerce_quotes.rules.inc
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_quotes_rules_event_info() {
  $events = array();

  $events['commerce_quotes_requested'] = array(
    'label' => t('Quote request sent by customer'),
    'group' => t('Commerce Quote'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Requested quote', array(), array('context' => 'a drupal commerce order')),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_quotes_sent'] = array(
    'label' => t('Quote sent to a customer'),
    'group' => t('Commerce Quote'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Sent quote', array(), array('context' => 'a drupal commerce order')),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_quotes_accepted'] = array(
    'label' => t('Quote accepted by the customer'),
    'group' => t('Commerce Quote'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Reviewed quote', array(), array('context' => 'a drupal commerce order')),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_quotes_declined'] = array(
    'label' => t('Quote declined by the customer'),
    'group' => t('Commerce Quote'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Reviewed quote', array(), array('context' => 'a drupal commerce order')),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}
