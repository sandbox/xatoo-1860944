<?php
/**
 * @file
 * Defines the pane which is shown during checkout
 */

/**
 * Settings_form callback.
 *
 * Do not define any settings. Note: This is NOT the form shown on the pane.
 */
function commerce_quotes_pane_order_options_settings_form($checkout_pane) {
  return array();
}

/**
 * Checkout_form callback.
 *
 * Note: This is the form shown on the pane.
 */
function commerce_quotes_pane_order_options_checkout_form($form, &$form_state, $checkout_pane, $entity) {
  $id = $checkout_pane['pane_id'];
  $pane_form = array('#parents' => array($id));
  $default = isset($form_state['values'][$id]['quotation_form']) ? $form_state['values'][$id]['quotation_form'] : NULL;

  $pane_form['quotation_form'] = array(
    '#type' => 'radios',
    '#title' => t('Please select an option'),
    '#default_value' => $default,
    '#required' => TRUE,
    '#options' => array(
      'quote' => t('Request quote'),
      'order' => t('Place order'),
    ),
  );

  return $pane_form;
}

/**
 * Checkout_form_validate callback.
 */
function commerce_quotes_pane_order_options_checkout_form_validate($form, &$form_state, $checkout_pane, $entity) {
  $id = $checkout_pane['pane_id'];

  if (!isset($form_state['values'][$id]['quotation_form'])) {
    form_set_error('quotation_form', t('Please select an option'));
    return FALSE;
  }

  return TRUE;
}

/**
 * Checkout_form_submit callback.
 */
function commerce_quotes_pane_order_options_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  $id = $checkout_pane['pane_id'];

  if ($form_state['values'][$id]['quotation_form'] == 'quote') {

    $form_state['values']['send_quote'] = TRUE;

    // Process quote after the default submit handler, that way the order is allready saved.
    $form_state['submit_handlers'][] = 'commerce_quotes_process_submitted_quote';

    // Ugly hack to prevent the default redirect from happening
    // TODO: Do this the Drupal way, whatever that might be.
    unset($form_state['triggering_element']['#array_parents'][1]);
  }
}

function commerce_quotes_process_submitted_quote($form, &$form_state) {
  if (isset($form_state['values']['send_quote'])) {
    $order = commerce_order_load($form_state['order']->order_id);

    // Invoke rules to update the quote status and send out mails.
    rules_invoke_all('commerce_quotes_requested', $order);

    // Redirect to the next checkout page.
    drupal_set_message(t('The quotation request has been sent. You will soon receive a response.'));

    global $user;
    $form_state['redirect'] = ($user->uid) ? ('user/' . $user->uid . '/orders/' . $order->order_number) : '<front>';
  }
}
