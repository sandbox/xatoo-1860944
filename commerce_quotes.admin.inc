<?php
/**
 * @file
 * Administrative callbacks and form builder functions for Commerce Quotes.
 */

/**
 * Page callback
 */
function commerce_quotes_edit_quote($order) {
  // Only allow editting of quotation requests.
  if ($order->status != 'request_for_quotation') {
    drupal_access_denied();
  }

  // Set the title and the breadcrumb to something decent.
  commerce_quotes_edit_order_set_title();

  // TODO:
  // - Some kind of order editor but then better than this:
  // - Add submit handler to change order state.
  module_load_include('inc', 'commerce_order', 'includes/commerce_order.forms');

  // Get the order edit form
  $form = drupal_get_form('commerce_order_ui_order_form', $order);

  $form['actions']['submit']['#value'] = t('Send quote');

  return $form;
}

/**
 * Set the breadcrumb and title when editting a quotation request.
 */
function commerce_quotes_edit_order_set_title() {
  drupal_set_title(t('Reply to a request'));

  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Quotation requests'), 'all_quotes'),
  );

  drupal_set_breadcrumb($breadcrumb);
}

/**
 * Commerce Stock admin form.
 */
function commerce_quotes_admin_form($form, &$form_state) {
  $form = array();

  $form['commerce_quotes_quote_from_cart'] = array(
    '#type' => 'checkbox',
    '#title' => t('Quote from cart'),
    '#default_value' => variable_get('commerce_quotes_quote_from_cart', FALSE),
    '#description' => t("Force users to request a quote from the cart page and prevent continuing to checkout."),
  );

  return system_settings_form($form);
}
