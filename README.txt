CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * References

INTRODUCTION
------------

Current Maintainer: Sander Toonen (xatoo) <s.toonen@gmail.com>

Commerce Quotes is a module for Drupal Commerce that will allow users to send
a request for quotation (RFQ) instead of placing an order during checkout. The
module provides a number of methods to determine whether an order should become 
an RFQ. Conditions might be that the order contains products without a price or
that the customer checked a box indicating he or she would like to place a RFQ
during checkout. Alternatively you can specify your own conditions using the
amazing Rules module.

Shop administrators can get an overview of all incoming RFQs from where they are
able to send an altered version of the RFQ as a quote back to the customer. The
customer then receives an email notification after which they might accept or
decline a quote. Once accepted an order is placed and normal checkout will be
resumed.


INSTALLATION
------------



CONFIGURATION
-------------



REFERENCES
----------

 1. #1326212: The issue where this module was born.

