<?php
/**
 * @file
 * commerce_quotes.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_quotes_default_rules_configuration() {
  // Store the customer profile entity info for use in default rules.
  $customer_profile_entity_info = entity_get_info('commerce_customer_profile');

  $rules = array();

  // Reaction rule to update an order to the status 'request_for_quotation'
  // upon quote request completion.
  $rule = rules_reaction_rule();

  $rule->label = t('Update the quote status on request completion');
  $rule->active = TRUE;

  $rule
    ->event('commerce_quotes_requested')
    ->action('commerce_order_update_status', array(
      'commerce_order:select' => 'commerce-order',
      'order_status' => 'request_for_quotation',
    ));

  $rules['commerce_quotes_request_status_update'] = $rule;

  // Add a reaction rule to update an order to the status 'quotation_response'
  // upon quote reply completion.
  $rule = rules_reaction_rule();

  $rule->label = t('Update the quote status on reply completion');
  $rule->active = TRUE;

  $rule
    ->event('commerce_quotes_sent')
    ->action('commerce_order_update_status', array(
      'commerce_order:select' => 'commerce-order',
      'order_status' => 'quotation_response',
    ));

  $rules['commerce_quotes_reply_status_update'] = $rule;

  // Add a reaction rule to update an order to the status 'quotation_response'
  // upon quote reply completion.
  $rule = rules_reaction_rule();

  $rule->label = t('Update the quote status when accepting a quote');
  $rule->active = TRUE;

  $rule
    ->event('commerce_quotes_accepted')
    ->action('commerce_order_update_status', array(
      'commerce_order:select' => 'commerce-order',
      'order_status' => 'pending',
    ));

  $rules['commerce_quotes_accept_status_update'] = $rule;

  // Add a reaction rule to update an order to the status 'quotation_response'
  // upon quote reply completion.
  $rule = rules_reaction_rule();

  $rule->label = t('Update the quote status when declining a quote');
  $rule->active = TRUE;

  $rule
    ->event('commerce_quotes_declined')
    ->action('commerce_order_update_status', array(
      'commerce_order:select' => 'commerce-order',
      'order_status' => 'canceled_quote',
    ));

  $rules['commerce_quotes_decline_status_update'] = $rule;

  // Add a reaction rule that creates a new user account during quote request
  // completion if the customer specified a non-existent e-mail address. The
  // default functionality is to create an active user account with the e-mail
  // for administrator created accounts and will always assume the need for
  // e-mail verification for setting a password.
  $rule = rules_reaction_rule();

  $rule->label = t('Create a new account for an anonymous quote request');
  $rule->active = TRUE;

  $rule
    ->event('commerce_quotes_requested')
    ->condition('data_is', array(
      'data:select' => 'commerce-order:uid',
      'op' => '==',
      'value' => '0',
    ))
    ->condition(rules_condition('entity_exists', array(
      'type' => 'user',
      'property' => 'mail',
      'value:select' => 'commerce-order:mail',
    ))->negate())
    ->condition('data_is', array(
      'data:select' => 'commerce-order:type',
      'op' => '==',
      'value' => 'commerce_order',
    ))
    ->action('entity_create', array(
      'type' => 'user',
      'param_name:select' => 'commerce-order:mail-username',
      'param_mail:select' => 'commerce-order:mail',
      'entity_created:label' => t('Created account'),
      'entity_created:var' => 'account_created',
    ))
    ->action('data_set', array(
      'data:select' => 'account-created:status',
      'value' => 1,
    ))
    ->action('entity_save', array(
      'data:select' => 'account-created',
      'immediate' => 1,
    ))
    ->action('entity_query', array(
      'type' => 'user',
      'property' => 'mail',
      'value:select' => 'commerce-order:mail',
      'limit' => 1,
      'entity_fetched:label' => t('Fetched account'),
      'entity_fetched:var' => 'account_fetched',
    ));

  // Build a loop that send the account notification e-mail and updates the
  // order and customer profile uids with the uid from the fetched user account.
  $loop = rules_loop(array(
    'list:select' => 'account-fetched',
    'item:var' => 'list_item',
    'item:label' => t('Current list item'),
    'item:type' => 'user',
  ))
    ->action('send_account_email', array(
      'account:select' => 'list-item',
      'email_type' => 'register_admin_created',
    ))
    ->action('data_set', array(
      'data:select' => 'commerce-order:uid',
      'value:select' => 'list-item:uid',
    ));

  // Accommodate any profile types referenced by the order.
  foreach ($customer_profile_entity_info['bundles'] as $type => $data) {
    $instance = field_info_instance('commerce_order', 'commerce_customer_' . $type, 'commerce_order');

    if (!empty($instance)) {
      $loop
        ->action('data_set', array(
          'data:select' => 'commerce-order:' . strtr('commerce-customer-' . $type, '_', '-') . ':uid',
          'value:select' => 'list-item:uid',
        ));
    }
  }

  // Add the loop to the rule as an action.
  $rule->action($loop);

  // Adjust the weight so this rule executes after the one checking for a pre-
  // existing user account.
  $rule->weight = 2;

  $rules['commerce_quotes_new_account'] = $rule;

  // Add a reaction rule to send order e-mail upon checkout completion.
  $rule = rules_reaction_rule();

  $rule->label = t('Send an quote notification e-mail');
  $rule->active = TRUE;

  $rule
    ->event('commerce_quotes_requested')
    ->action('mail', array(
      'to:select' => 'commerce-order:mail',
      'subject' => t('Quote [commerce-order:order-number] at [site:name]'),
      'message' => t("Thanks for your quote request [commerce-order:order-number] at [site:name].\n\nIf this is your first quote request with us, you will receive a separate e-mail with login instructions. You can view your quote history with us at any time by logging into our website at:\n\n[site:login-url]\n\nYou can find the status of your current request at:\n\n[commerce-order:customer-url]\n\nPlease contact us if you have any questions about your quote request."),
      'from' => '',
    ));

  // Adjust the weight so this rule executes after the order has been updated to
  // the proper user account.
  $rule->weight = 4;

  $rules['commerce_quotes_request_email'] = $rule;

  return $rules;
}
